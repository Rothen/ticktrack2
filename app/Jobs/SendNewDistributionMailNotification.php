<?php

namespace App\Jobs;

use App\Mail\NewDistributionNotification;
use App\User;
use App\UserNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendNewDistributionMailNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $distribution;

    /**
     * Create a new job instance.
     *
     * @param $distribution
     */
    public function __construct($distribution)
    {
        $this->distribution = $distribution;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $attuids = UserNotification::where('mail', '=', true)->pluck('attuid');

        $usersToNotify = User::whereIn('attuid', $attuids)->pluck('email');

        Mail::to('donotreply2@att.com')->bcc($usersToNotify)->send((new newDistributionNotification($this->distribution)));
    }
}
