<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'logs';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    // make these dates automatically to Carbon instance -> can be easily formatted in blade
    protected $dates = ['created_at', 'updated_at' ];
}
