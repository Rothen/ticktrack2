<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    protected $primaryKey = 'attuid';
    public $incrementing = false; // By default, Eloquent assumes primary keys are integers and will automatically cast them to integers - https://github.com/laravel/docs/commit/b8c09f70d39f8c6891978a54c250a2fe9ad94947
    protected $table = "users";
    protected $guarded = ['created_at', 'updated_at'];
    protected $hidden = ['password', 'remember_token'];

    // make these dates automatically to Carbon instance -> can be easily formatted in blade
    protected $dates = ['created_at', 'updated_at', 'last_login'];

    protected $casts = [
        'enabled' => 'boolean'
    ];

    // implicit model binding to use a database column other than id when retrieving models,
    // this getRouteKeyName method overrides the default id column
    public function getRouteKeyName()
    {
        return 'attuid';
    }

    /* ATTRIBUTES */

    public function setAttuidAttribute($value)
    {
        $this->attributes['attuid'] = strtolower($value);
        $this->attributes['email'] = strtolower($value) . '@att.com';
    }

    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = ucwords(strtolower($value));
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = ucwords(strtolower($value));
        $this->attributes['full_name'] = $this->attributes['first_name'] .' ' . $this->attributes['last_name'];
    }

    /* RELATIONSHIPS */
    public function userNotifications(){
        return $this->belongsTo(UserNotification::class, 'attuid', 'attuid');
    }

    /* OTHERS */

    public static function searchUser($searchString) {

        $result = self::with('roles'.'teams')
            ->where('last_name', 'like', '%' . $searchString . '%')
            ->orWhere('first_name', 'like', '%' . $searchString . '%')
            ->orWhere('attuid', 'like', '%' . $searchString . '%')
            ->orderBy('attuid')
            ->limit(5)
            ->get();

        return $result;

    }
}
