<?php

namespace App\Providers;

use App\Distribution;
use App\Exception;
use App\Permission;
use App\Role;
use App\User;
use App\UserNotification;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        // use model binding with attuid instead of id
        Route::bind('attuid', function($value) {
            return (new User)->where('attuid', '=', $value)->with('roles.permissions', 'userNotifications')->first() ?? abort(404);
        });

        // PERMISSION - use model binding
        Route::bind('permission', function($value) {
            return (new Permission)->where('id', '=', $value)->with('roles')->first() ?? abort(404);
        });

        // ROLE - use model binding
        Route::bind('role', function($value) {
            return (new Role)->where('name', '=', $value)->with('permissions')->first() ?? abort(404);
        });

        // UserNotification - use model binding
        Route::bind('user', function($value) {
            return (new UserNotification)->where('attuid', '=', $value)->with('users')->first() ?? abort(404);
        });

        // Distribution - use model binding
        Route::bind('distribution', function($value) {
            return (new Distribution)->where('name', '=', $value)->first() ?? abort(404);
        });

        // Exception - use model binding
        Route::bind('exception', function($value) {
            return (new Exception)->where('id', '=', $value)->first() ?? abort(404);
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
