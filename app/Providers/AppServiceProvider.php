<?php

namespace App\Providers;

use App\Distribution;
use App\Exception;
use App\Permission;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /* SHARE WITH ALL VIEWS */
        view()->composer('*', function ($view)
        {
            // for view where user isn't logged in (for example login view) don't try to provide the data
            if (Auth::user()) {
                $view
                    ->with('authenticated_user', Auth::user()  )
                    ->with('users_permissions', Auth::user()->allPermissions()->pluck('name')->toArray() );
            }

        });

        /* BLADE CONDITIONS */

        Blade::if('has_permission', function($permissions, $user_permissions) {
            foreach (explode('|', $permissions) as $permission) {
                if (in_array($permission, $user_permissions)) {
                    return true;
                }
            }
        });

        /* VALIDATORS */

        Validator::extend('admin_assignment_check', function ($attribute, $value, $parameters, $validator) {

            foreach ($value as $role) {
                if ($role === 'admin') {
                    if (Auth::user()->hasRole('admin')) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            }

        });

        Validator::extend('admin_permissions_check', function ($attribute, $value, $parameters, $validator) {

            $admin_permissions = (new Permission)
                ->whereIn('name', array_column($value, 'name'))
                ->where('category', '=', 'admin')
                ->count();

            if( $admin_permissions > 0 ) {
                if(Auth::user()->hasRole('admin')){
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }

        });

        Validator::extend('check_role_name_setter', function ($attribute, $value, $parameters, $validator) {

            if(Role::where('name', '=', str_replace([' '], '_', strtolower($value)))->count() > 0) {
                return false;
            } else {
                return true;
            }

        });

        Validator::extend('role_name_duplication', function ($attribute, $value, $parameters, $validator) {

            $name = $parameters[0];
            $initialDisplayName = $parameters[1];

            if($name !== str_replace([' '], '_', strtolower($value))) {
                if(Role::where('name', '=', str_replace([' '], '_', strtolower($value)))->count() > 0) {
                    return false;
                }
            }

            if(Role::where('display_name', '=', $value)->where('display_name', '=', $initialDisplayName)->count() === 1) {
                return true;
            } elseif(Role::where('display_name', '=', $value)->count() > 0) {
                return false;
            } else {
                return true;
            }

        });

        Validator::extend('check_distribution_name_setter', function ($attribute, $value, $parameters, $validator) {

            if(Distribution::where('name', '=', str_replace([' '], '_', strtolower($value)))->count() > 0) {
                return false;
            } else {
                return true;
            }

        });

        Validator::extend('distribution_name_duplication', function ($attribute, $value, $parameters, $validator) {

            $name = $parameters[0];
            $initialDisplayName = $parameters[1];

            if($name !== str_replace([' '], '_', strtolower($value))) {
                if(Distribution::where('name', '=', str_replace([' '], '_', strtolower($value)))->count() > 0) {
                    return false;
                }
            }

            if(Distribution::where('display_name', '=', $value)->where('display_name', '=', $initialDisplayName)->count() === 1) {
                return true;
            } elseif(Distribution::where('display_name', '=', $value)->count() > 0) {
                return false;
            } else {
                return true;
            }

        });

        Validator::extend('self_parent', function ($attribute, $value, $parameters, $validator) {

            if((int)$value === (int)$parameters[0]) {
                return false;
            } else {
                return true;
            }

        });

        Validator::extend('exception_duplication', function ($attribute, $value, $parameters, $validator) {

            if(Exception::where('attuid', '=', $value)->where('distribution', '=', $parameters[0])->first()) {
                return false;
            } else {
                return true;
            }

        });

    }
}
