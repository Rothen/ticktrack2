<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Exception extends Model
{
    protected $table = "exceptions";
    protected $guarded = ['id', 'created_at', 'updated_at'];

    // make these dates automatically to Carbon instance -> can be easily formatted in blade
    protected $dates = ['created_at', 'updated_at'];

    // implicit model binding to use a database column other than id when retrieving models,
    // this getRouteKeyName method overrides the default id column
    public function getRouteKeyName()
    {
        return 'exception';
    }

    /* RELATIONS */

    public function distributions()
    {
        return $this->belongsTo(Distribution::class, 'distribution', 'name');
    }

    /* SETTERS */
    public function setAttuidAttribute($value)
    {
        $employee = (new Webphone)->where('SBCUID' , $value)->first();

        $this->attributes['attuid'] = strtolower($value);
        $this->attributes['full_name'] = ucwords(strtolower($employee->FIRST_NAME)) . ' ' . ucwords(strtolower($employee->LAST_NAME));
        // this condition is here to run when seeding
        $this->attributes['approver_attuid'] = (Auth::user()) ? Auth::user()->attuid : 'rr891c';
        $this->attributes['approver_full_name'] = (Auth::user()) ? Auth::user()->full_name : 'Robert Rothenstein';
    }
}
