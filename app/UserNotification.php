<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $table = "user_notifications";
    protected $guarded = ['id', 'created_at', 'updated_at'];

    // make these dates automatically to Carbon instance -> can be easily formatted in blade
    protected $dates = [ 'created_at', 'updated_at'];

    protected $casts = [
        'q' => 'boolean',
        'mail' => 'boolean',
        'confirmed' => 'boolean'
    ];

    // implicit model binding to use a database column other than id when retrieving models,
    // this getRouteKeyName method overrides the default id column
    public function getRouteKeyName()
    {
        return 'attuid';
    }

    /* RELATIONSHIPS */
    public function users() {
        return $this->belongsTo(User::class, 'attuid', 'attuid');
    }
}
