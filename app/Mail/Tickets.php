<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Tickets extends Mailable
{
    use Queueable, SerializesModels;

    protected  $data;
    protected  $attachment;

    /**
     * Create a new message instance.
     *
     * @param $data
     * @param $attachment
     */
    public function __construct($data, $attachment)
    {
        $this->data = $data;
        $this->attachment = $attachment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.emailWithAttachedTickets')
            ->with('records', $this->data)
            ->subject('Kupóny do Cinema City')
            ->attachData($this->attachment->output(), 'tickets.pdf');
            ;
    }
}
