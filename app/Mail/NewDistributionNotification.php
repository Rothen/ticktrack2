<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewDistributionNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected  $distribution;

    /**
     * Create a new message instance.
     *
     * @param $distribution
     */
    public function __construct($distribution)
    {
        $this->distribution = $distribution;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.newDistributionNotification')
            ->with('distribution', $this->distribution)
            ->subject('Upozornenie na nové kupóny do Cinema City')
            ;
    }
}
