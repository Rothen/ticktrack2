<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distribution extends Model
{
    protected $primaryKey = 'name';
    public $incrementing = false; // By default, Eloquent assumes primary keys are integers and will automatically cast them to integers - https://github.com/laravel/docs/commit/b8c09f70d39f8c6891978a54c250a2fe9ad94947
    protected $table = "distributions";
    protected $guarded = ['name', 'created_at', 'updated_at'];

    // make these dates automatically to Carbon instance -> can be easily formatted in blade
    protected $dates = ['from', 'created_at', 'updated_at'];

    // implicit model binding to use a database column other than id when retrieving models,
    // this getRouteKeyName method overrides the default id column
    public function getRouteKeyName()
    {
        return 'distribution';
    }

    /* RELATIONS */

    public function exceptions()
    {
        return $this->hasMany(Exception::class, 'distribution', 'name');
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'distribution', 'name');
    }

    /* SETTERS */

    public function setDisplayNameAttribute($value)
    {
        $this->attributes['display_name'] = $value;
        $this->attributes['name'] = str_replace([' '], '_', strtolower($value));
    }
}
