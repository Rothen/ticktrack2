<?php

namespace App\Http\Controllers;

use App\Distribution;
use App\Exception;
use App\Http\Requests\SaveExceptionRequest;
use App\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExceptionController extends MyController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function actual()
    {
        $actual_distribution = Distribution::where('status', '=', 'open')->first();

        return Exception::where('distribution', '=', $actual_distribution->name)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveExceptionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveExceptionRequest $request)
    {
        $values = $request->all();

        $values['approver_attuid'] = Auth::user()->attuid;
        $values['approver_full_name'] = Auth::user()->full_name;

        $exception = Exception::create($values);

        Log::create([
            'type' => 'exception_creation',
            'triggeredByAttuid' => Auth::user()->attuid,
            'triggeredByName' => Auth::user()->full_name,
            'action' => 'exception created',
            'description' => 'New exception for ' . $exception->full_name . ' added by ' . $exception->approver_full_name . '.',
        ]);

        return $exception;
    }

    /**
     * Display the specified resource.
     *
     * @param Distribution $distribution
     * @return Exception
     */
    public function show(Distribution $distribution)
    {
        return $distribution->exceptions;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Exception $exception
     * @return Exception
     */
    public function update(Request $request, Exception $exception)
    {
        $values = $request->all();

        $values['approver_attuid'] = Auth::user()->attuid;
        $values['approver_full_name'] = Auth::user()->full_name;

        $exception->update($values);

        Log::create([
            'type' => 'exception_update',
            'triggeredByAttuid' => Auth::user()->attuid,
            'triggeredByName' => Auth::user()->full_name,
            'action' => 'exception updated',
            'description' => 'Exception for ' . $exception->full_name . ' updated by ' . Auth::user()->full_name . '. Values: ' . http_build_query($values, '', ' / ') . '.',
        ]);

        return $exception;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exception $exception
     * @return Exception
     * @throws \Exception
     */
    public function destroy(Exception $exception)
    {
        $exception->delete();

        Log::create([
            'type' => 'exception_deletion',
            'triggeredByAttuid' => Auth::user()->attuid,
            'triggeredByName' => Auth::user()->full_name,
            'action' => 'exception deletion',
            'description' => 'Exception for ' . $exception->full_name . ' deleted by ' . Auth::user()->full_name
        ]);

        return $exception;
    }

    public function copyExceptions(Request $request) {
        $previousExceptions = Distribution::where('name', '=', $request['oldDistribution']['name'])->first()->exceptions;
        $newDistribution = Distribution::where('status', '=', 'new')->first();
        if($newDistribution) {
            $newDistributionAttuids = $newDistribution->exceptions->pluck('attuid')->toArray();
            $existCount = 0;
            foreach ($previousExceptions as $previousException) {
                if (!in_array($previousException['attuid'], $newDistributionAttuids)) {
                    $values['attuid'] = $previousException['attuid'];
                    $values['full_name'] = $previousException['full_name'];
                    $values['description'] = $previousException['description'];
                    $values['distribution'] = $newDistribution['name'];
                    $values['tickets_per_emp'] = $previousException['tickets_per_emp'];
                    $values['approver_attuid'] = Auth::user()->attuid;
                    $values['approver_full_name'] = Auth::user()->full_name;

                    Exception::create($values);
                } else {
                    $existCount++;
                }
            }

            if ($existCount === 0) {
                $copyText = 'No duplications were found.';
            } else {
                $copyText = $existCount . ' duplications were found and were NOT copied.';
            }

            Log::create([
                'type' => 'exception_copy',
                'triggeredByAttuid' => Auth::user()->attuid,
                'triggeredByName' => Auth::user()->full_name,
                'action' => 'exception copy',
                'description' => 'Exceptions from ' . $request['oldDistribution']['display_name'] . ' were copied to ' . $newDistribution->display_name . ' by ' . Auth::user()->full_name . '. ' . $copyText
            ]);

            return $copyText;
        } else {
            // this text is subject to condition in javascript, therefore DO NOT CHANGE
            return 'No distribution in status "new" exists';
        }
    }
}
