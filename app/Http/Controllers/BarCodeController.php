<?php

namespace App\Http\Controllers;

use App\Mail\Tickets;
use App\Ticket;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Milon\Barcode\DNS1D;

class BarCodeController extends MyController
{
    public function show() {
        $tickets = Ticket::userTicketsUsageOverview();
        $barCodes = [];

        foreach ($tickets['new'] as $ticket) {
            $record = DNS1D::getBarcodeHTML($ticket['ticketId'], "EAN13", 3, 93, 'black', false);
            array_push($barCodes, ['barCode' => $record, 'number' => $ticket['ticketId']]);
        }

        return $barCodes;
    }

    public function sendBarCodesInMail() {
        $barCodes = self::show();
        $attachment = self::createBarCodesToPDF();

        Mail::to(Auth::user()->email)->send((new Tickets($barCodes, $attachment)));

        return 'ready';
    }

    public function createBarCodesToPDF()
    {
        $barCodes = self::show();
        $pdf = PDF::loadView('emails.tickets', ['records' => $barCodes ]);

        return $pdf;
    }

    public function generateBarCodesToPDF()
    {
        $pdf = self::createBarCodesToPDF();

        return $pdf->download('tickets.pdf');
    }

}
