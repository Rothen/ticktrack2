<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // this overwrites the Laravel's default email address as the username login credential
    public function username()
    {
        return 'attuid';
    }

    protected function credentials(Request $request) {
        $credentials = $request->only($this->username(), 'password');
        $credentials['enabled'] = 1;
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return $credentials;
        } else {
            abort(403, 'You\'re not authorized to see these data or your access might have been disabled. Please, contact your manager. Admin will NOT be able to help you, as Admin can NOT decide whether you should see these data.');
        }
    }

}
