<?php

namespace App\Http\Controllers;

use App\Imports\TicketsImport;
use App\Log;
use App\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class TicketController extends MyController
{
    public function retrieveEmployeeEligibleTickets() {
        return Ticket::retrieveEmployeeEligibleTickets();
    }

    public function userTicketsUsageOverview() {
        return Ticket::userTicketsUsageOverview();
    }

    public function getTickets() {
        return Ticket::getTickets();
    }

    public function model(array $row)
    {
        return new Ticket([
            'ticketId' => $row[0],
        ]);
    }
    /**
     * @param Request $request
     * @return string
     * @throws \Illuminate\Validation\ValidationException
     */
    function import(Request $request)
    {

        $this->validate($request, [
            'file'  => 'required|mimes:xls,xlsx'
        ]);

        $data = (new TicketsImport)->toArray($request->file('file'));

        $insert_data = [];

        $used_recent_tickets = [];
        $unused_recent_tickets = [];

        $recent_tickets_array = Ticket::where('bought', '>', Carbon::now()->subMonths(18))->pluck('ticketId')->toArray();
        $recent_tickets_array_invalid = Ticket::where('bought', '>', Carbon::now()->subMonths(18))
                                        ->where(function ($query) {
                                            $query->where('used', '!=', null)
                                                ->orWhere('expiration', '<', Carbon::now());
                                        })
                                        ->pluck('ticketId')
                                        ->toArray();
        $recent_tickets_array_valid = Ticket::where('used', '=', null)
                                        ->where('expiration', '>', Carbon::now())
                                        ->pluck('ticketId')
                                        ->toArray();

        if(count($data) > 0)
        {
            foreach($data as $key => $value)
            {
                foreach($value as $row)
                {
                    if(in_array($row['ticketid'], $recent_tickets_array)) {
                        if(in_array($row['ticketid'], $recent_tickets_array_valid)) {
                            array_push($unused_recent_tickets, $row['ticketid']);
                        } else if (in_array($row['ticketid'], $recent_tickets_array_invalid)) {
                            array_push($used_recent_tickets, $row['ticketId']);
                            array_push($insert_data, [
                                'ticketId'  => $row['ticketid'],
                                'expiration'   => Carbon::now()->endOfYear(),
                                'bought'   => Carbon::now()
                            ]);
                        }
                    } else {
                        array_push($insert_data, [
                            'ticketId' => $row['ticketid'],
                            'expiration' => ($row['expiration']) ? $row['expiration'] : Carbon::now()->endOfYear(),
                            'bought' => ($row['bought']) ? $row['bought'] : Carbon::now()
                        ]);
                    }
                }
            }

            if(!empty($insert_data))
            {
                DB::table('tickets')->insert($insert_data);
            }
        }

        $text = count($used_recent_tickets) . ' tickets with same ID as previously used or expired tickets uploaded (last 18 months). ' . count($unused_recent_tickets) . ' tickets which are already in the database as valid unused tickets were NOT uploaded. Totally were ' . count($insert_data) . ' tickets uploaded.';

        Log::create([
            'type' => 'import',
            'triggeredByAttuid' => Auth::user()->attuid,
            'triggeredByName' => Auth::user()->full_name,
            'action' => 'tickets import',
            'description' => $text . ' Import done by ' . Auth::user()->full_name . ' (' . Auth::user()->attuid . ')',
        ]);

        return $text;
    }
}
