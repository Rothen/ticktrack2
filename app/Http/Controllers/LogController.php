<?php

namespace App\Http\Controllers;

use App\Log;
use Illuminate\Http\Request;

class LogController extends MyController
{
    public function store(Request $request) {
        Log::create($request->all());
    }
}
