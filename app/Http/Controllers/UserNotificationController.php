<?php

namespace App\Http\Controllers;

use App\UserNotification;
use Illuminate\Http\Request;

class UserNotificationController extends MyController
{
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'attuid' => 'required|unique:user_notifications,attuid|max:10',
        ]);

        $userNotification = UserNotification::create($request->all());

        return $userNotification;
    }

    public function update(Request $request, $userNotification)
    {
        $updateValues = $request->all();
        $updateValues['confirmed'] = true;
        $userNotification->update($updateValues);

        return $userNotification;
    }
}
