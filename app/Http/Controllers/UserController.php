<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Jobs\SendNotification;
use App\Log;
use App\Mail\Notification;
use App\User;
use App\Webphone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserController extends MyController
{
    /**
     * Display a listing of the resource.
     *
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return User::with('roles')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveUserRequest $request)
    {
        $user = User::create($request->except(['role']));

        $user->attachRoles($request['role']);

        Log::create([
            'type' => 'sign_in',
            'triggeredByAttuid' => Auth::user()->attuid,
            'triggeredByName' => Auth::user()->full_name,
            'action' => 'new user signed in',
            'description' => 'New user - ' . $user->full_name . ' (' . $user->attuid . ')',
        ]);

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param $attuid
     * @return \Illuminate\Http\Response
     */
    public function show($attuid)
    {
        return $attuid;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $user)
    {
        $user->update($request->except(['role']));

        $user->syncRoles($request['role']);

        return $user;
    }

    public function disable(Request $request, $user)
    {
        $user->update($request->only(['enabled']));

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return void
     */
    public function destroy($user)
    {
        //
    }

    public function searchEmployee($searchString = ''){
        return Webphone::searchEmployee($searchString);
    }

    public function searchUser($searchString = ''){
        return User::searchUser($searchString);
    }

    public function getUser() {
        return Auth::user()->load('userNotifications');
    }

}
