<?php

namespace App\Http\Controllers;

use App\Distribution;
use App\Http\Requests\SaveDistributionRequest;
use App\Jobs\SendNewDistributionMailNotification;
use App\Log;
use App\Webphone;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DistributionController extends MyController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Distribution::orderBy('created_at', 'desc')
            ->withCount(['tickets as tickets_picked' => function (Builder $query) {
                $query->where('attuid', '!=', null);
            }])
            ->withCount(['tickets as tickets_used' => function (Builder $query) {
                $query->where('used', '!=', null);
            }])
            ->limit(30)
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveDistributionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveDistributionRequest $request)
    {
        $values = $request->all();

        $values['status'] = 'new';
        $values['total_num_of_emp'] = Webphone::where('WORK_CITY', '=', 'Bratislava')->count();

        $distribution = Distribution::create($values);

        Log::create([
            'type' => 'distribution_creation',
            'triggeredByAttuid' => Auth::user()->attuid,
            'triggeredByName' => Auth::user()->full_name,
            'action' => 'distribution created',
            'description' => 'New distribution - ' . $distribution->display_name . ' - created by ' . Auth::user()->full_name . '.',
        ]);

        return $distribution;
    }

    /**
     * Display the specified resource.
     *
     * @param $distribution
     * @return void
     */
    public function show($distribution)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $distribution
     * @return void
     */
    public function edit($distribution)
    {
        //
    }

    /**
     * @param Request $request
     * @param $distribution
     * @return mixed
     */
    public function update(Request $request, $distribution)
    {
        $values = $request->all();

        $sendNotification = false;

        if($request->status === 'open') {
            $values['from'] = Carbon::now();

            $previouslyOpenDistribution = Distribution::where('status', '=', 'open')->get();

            foreach ($previouslyOpenDistribution as $distributionToClose) {
                $distributionToClose->update(['status' => 'closed']);
            }

            $sendNotification = true;
        }

        $distribution->update($values);

        if($sendNotification) {
            SendNewDistributionMailNotification::dispatch(Distribution::where('status', '=', 'open')->first());
        }

        Log::create([
            'type' => 'distribution_update',
            'triggeredByAttuid' => Auth::user()->attuid,
            'triggeredByName' => Auth::user()->full_name,
            'action' => 'distribution updated',
            'description' => 'Distribution - ' . $distribution->display_name . ' - updated by ' . Auth::user()->full_name . '. Values: ' . http_build_query($values, '', ' / ') . '.',
        ]);

        return $distribution;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $distribution
     * @return void
     */
    public function destroy($distribution)
    {
        //
    }

    public function actualDistribution() {
        return Distribution::where('status', '=', 'open')->first();
    }
}
