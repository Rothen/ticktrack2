<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveRoleRequest;
use App\Permission;
use App\Role;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends MyController
{
    /**
     * Display a listing of the resource.
     *
     * @return Role[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        if(Auth::user()->hasRole('admin')) {
            return Role::all()->toArray();
        } else {
            return Role::where('name', '!=', 'admin')->get();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveRoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveRoleRequest $request)
    {
        $role = Role::create($request->except(['permissions']));

        foreach ($request->permissions as $permission) {
            $role->attachPermission($permission['name']);
        }

        return $role;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role $role
     * @return Role
     */
    public function show(Role $role)
    {
        return $role;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveRoleRequest $request
     * @param  \App\Role $role
     * @return void
     */
    public function update(SaveRoleRequest $request, Role $role)
    {
        // initialDisplayName is used in validation, but it does not exist on the model
        $role->update($request->except(['permissions', 'initialDisplayName']));

        foreach ($role->permissions as $permission) {
            $role->detachPermission($permission['name']);
        }

        foreach ($request->permissions as $permission) {
            $role->attachPermission($permission['name']);
        }

        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role $role
     * @return void
     * @throws \Exception
     */
    public function destroy(Role $role)
    {
        if($role->name !== 'admin') {
            $role->delete();
            return $role->display_name;
        }

    }

    /**
     * @param Role $role
     * @return mixed
     */
    public function assignees(Role $role)
    {
        return Role::getAssignees($role);
    }

    /**
     * @param Role $role
     * @return mixed
     */
    public function getRoleUnassignedPermissions(Role $role)
    {
        if(Auth::user()->hasRole('admin')) {
            $permissions = Permission::whereNotIn('name', $role->permissions()->pluck('name'))->get();
        } else {
            $permissions = Permission::whereNotIn('name', $role->permissions()->pluck('name'))->where('category', '!=', 'admin')->get();
        }

        return $permissions;
    }
}
