<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class SaveExceptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can('edit_exceptions');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $checks =[];
        $checks['attuid'] = 'required|exception_duplication:' . $this->get('distribution');
        $checks['distribution'] = 'required';
        $checks['tickets_per_emp'] = 'required|numeric';
        $checks['description'] = 'max:250';

        return $checks;
    }

    public function messages() {
        return [
            'attuid.required' => 'You must choose an employee.',
            'attuid.exception_duplication' => 'Employee has an exception already.',
            'distribution.required' => 'You must provide a distribution.',
            'tickets_per_emp.required' => 'You must provide number of tickets.',
            'description.max' => 'Description must not have more than 250 characters.',
        ];
    }
}
