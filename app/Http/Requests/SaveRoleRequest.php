<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SaveRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can('edit_roles');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->get('name'))
        {
            $name_rule = '';
            $display_name_rule = 'required|max:50|role_name_duplication:' . $this->get('name') . ',' . $this->get('initialDisplayName');
        } else {
            $name_rule = 'max:250|unique:roles,name';
            $display_name_rule = 'required|max:50|check_role_name_setter|unique:roles,display_name';
        }

        return [
            'name' => $name_rule,
            'display_name' => $display_name_rule,
            'description' => 'nullable|max:250',
            'permissions' => 'admin_permissions_check'
        ];
    }

    public function messages() {
        return [
            'name.required' => 'Please, enter role\'s name',
            'name.admin_name_check' => 'Name of \'admin\' role must not be changed',
            'display_name.required' => 'Please, enter role\'s name',
            'display_name.check_role_name_setter' => 'Similar role name exists. Please, enter different role name',
            'display_name.role_name_duplication' => 'Similar role name exists. Please, enter different role name',
            'permissions.required' => 'Please, select at least one permission',
            'permissions.admin_permissions_check' => 'You are not authorized to manage admin permissions'
        ];
    }
}
