<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SaveDistributionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can('edit_distributions');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->get('name'))
        {
            $name_rule = '';
            $display_name_rule = 'required|max:50|distribution_name_duplication:' . $this->get('name') . ',' . $this->get('initialDisplayName');
        } else {
            $name_rule = 'max:250|unique:distributions,name';
            $display_name_rule = 'required|max:50|check_distribution_name_setter|unique:distributions,display_name';
        }

        return [
            'name' => $name_rule,
            'display_name' => $display_name_rule,
            'tickets_per_emp' => 'min:1'
        ];
    }

    public function messages() {
        return [
            'name.required' => 'Please, enter distribution\'s name',
            'display_name.required' => 'Please, enter distribution\'s name',
            'display_name.check_distribution_name_setter' => 'Similar distribution name exists. Please, enter different distribution name',
            'display_name.distribution_name_duplication' => 'Similar distribution name exists. Please, enter different distribution name',
        ];
    }
}
