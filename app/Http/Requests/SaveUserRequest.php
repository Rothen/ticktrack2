<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SaveUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can('edit_users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $checks =[];
        $checks['attuid'] = 'required|unique:users,attuid';
        $checks['role'] = 'required|admin_assignment_check';
        $checks['team'] = 'sometimes|array';

        return $checks;
    }

    public function messages() {
        return [
            'attuid.required' => 'You must choose an employee.',
            'attuid.unique' => 'Employee is already among users.',
            'role.required' => 'You must select a role for the user.',
            'role.admin_assignment_check' => 'You don\'t have the permission to assign admin role.',
            'team.array' => 'Incorrect value format. Please reload and try again or contact the admin.'
        ];
    }
}
