<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Ticket extends Model
{
    protected $table = "tickets";
    protected $guarded = ['id', 'created_at', 'updated_at'];

    // make these dates automatically to Carbon instance -> can be easily formatted in blade
    protected $dates = ['expiration', 'used', 'bought', 'picked', 'created_at', 'updated_at'];


    // implicit model binding to use a database column other than id when retrieving models,
    // this getRouteKeyName method overrides the default id column
    public function getRouteKeyName()
    {
        return 'ticket';
    }

    /* RELATIONS */

    public function distributions()
    {
        return $this->belongsTo(Distribution::class, 'name', 'distribution');
    }

    /* OTHERS */

    public static function retrieveEmployeeEligibleTickets() {
        $distribution = Distribution::where('status', '=', 'open')->first();

        if($distribution) {
            $exception = Exception::where('distribution', '=', $distribution->name)
                ->where('attuid', '=', Auth::user()->attuid);

            if ($exception->count() > 0) {
                $employeeEligibleTickets = $exception->first()->tickets_per_emp;
            } else {
                $employeeEligibleTickets = $distribution->tickets_per_emp;
            }

            $picks = Ticket::where('distribution', '=', $distribution->name)
                ->where('attuid', '=', Auth::user()->attuid);

            $limit = $employeeEligibleTickets - $picks->count();

            if($limit < 0) {
                $limit = 0;
            }

            $records = Ticket::where('attuid', '=', null)
                ->orderBy('expiration')
                ->limit($limit)
                ->get()
                ->toArray();
        } else {
            $records = [];
        }

        return $records;
    }

    public static function userTicketsUsageOverview() {
        $usage1 = Ticket::where('attuid', '=', Auth::user()->attuid);
        $usage2 = clone $usage1;
        $tickets['used'] = $usage1->where(function ($query) {
                                $query->where('used', '!=', null)
                                    ->orWhere('expiration', '<', Carbon::now());
                            })->get();
        $tickets['new'] = $usage2->where('used', '=', null)->where('expiration', '>', Carbon::now())->get();

        return $tickets;
    }

    public static function getTickets() {
        $distribution = Distribution::where('status', '=', 'open')->first();

        $ticketsToGet = self::retrieveEmployeeEligibleTickets();

        foreach ($ticketsToGet as $ticket) {
            Ticket::find($ticket['id'])->update([
                'attuid' => Auth::user()->attuid,
                'distribution' => $distribution->name,
                'picked' => Carbon::now()
            ]);
        }

    }
}