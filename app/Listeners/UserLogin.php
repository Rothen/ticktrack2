<?php

namespace App\Listeners;

use App\Log;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class UserLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Auth::user()->update(['last_login' => Carbon::now() ]);

        Log::create([
            'type' => 'login',
            'triggeredByAttuid' => Auth::user()->attuid,
            'triggeredByName' => Auth::user()->full_name,
            'action' => 'user login',
            'description' => 'User ' . Auth::user()->full_name . ' (' . Auth::user()->attuid . ') logged into the app',
        ]);
    }
}
