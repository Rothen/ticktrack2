<?php

namespace App;

use Laratrust\Models\LaratrustPermission;

class Permission extends LaratrustPermission
{
    protected $table = "permissions";
    protected $guarded = ['id', 'created_at', 'updated_at'];

    // make these dates automatically to Carbon instance -> can be easily formatted in blade
    protected $dates = ['created_at', 'updated_at'];

    // implicit model binding to use a database column other than id when retrieving models,
    // this getRouteKeyName method overrides the default id column
    public function getRouteKeyName()
    {
        return 'permission';
    }

    /* RELATIONS */
    public function roles() {
        return $this->belongsToMany(Role::class, 'permission_role', 'permission_id', 'role_id');
    }

    /* SETTERS */

    public function setDisplayNameAttribute($value)
    {
        $this->attributes['display_name'] = $value;
        $this->attributes['name'] = str_replace([' '], '_', strtolower($value));
    }
}
