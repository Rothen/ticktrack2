<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webphone extends Model
{
    protected $connection = "sqlsrv_webphone";

    protected $table = "PHONE";

    /* RELATIONSHIPS */

    // relation with users data
    public function users(){
        return $this->hasOne(User::class, 'attuid', 'SBCUID');
    }

    /* ATTRIBUTES */
    public function getFullNameWithAttuidAttribute()
    {
        $full_name = $this->FIRST_NAME . ' ' . $this->LAST_NAME;
        $full_name = ucwords(strtolower($full_name));
        $full_name_with_attuid = $full_name . ', ' . strtolower($this->SBCUID);

        return $full_name_with_attuid;
    }

    /* SEARCH */

    public static function searchEmployee($searchString) {

        $result = self::where(function ($query) use ($searchString) {
                $query->where('LAST_NAME', 'like', '%' . $searchString . '%')
                ->orWhere('FIRST_NAME', 'like', '%' . $searchString . '%')
                ->orWhere('SBCUID', 'like', '%' . $searchString . '%');
            })
            ->where('WORK_CITY', '=', 'Bratislava')
            ->orderBy('SBCUID')
            ->limit(5)
            ->get();

        return $result;

    }
}
