<?php

namespace App;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    protected $table = "roles";
    protected $guarded = ['id', 'created_at', 'updated_at'];

    // make these dates automatically to Carbon instance -> can be easily formatted in blade
    protected $dates = ['created_at', 'updated_at'];

    // implicit model binding to use a database column other than id when retrieving models,
    // this getRouteKeyName method overrides the default id column
    public function getRouteKeyName()
    {
        return 'role';
    }

    /* RELATIONS */

    public function users() {
        return $this->belongsToMany(User::class, 'role_user', 'role_id', 'attuid');
    }


    /* SETTERS */

    public function setDisplayNameAttribute($value)
    {
        $this->attributes['display_name'] = $value;

        if(array_key_exists('name', $this->attributes)) {
            if ($this->attributes['name'] !== 'admin') {
                $this->attributes['name'] = str_replace([' '], '_', strtolower($value));
            }
        } else {
            $this->attributes['name'] = str_replace([' '], '_', strtolower($value));
        }
    }

    /* Other */

    public static function getAssignees(Role $role) {
        return $role->users;
    }

}
