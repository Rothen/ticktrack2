<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $attuid = $faker->randomLetter . $faker->randomLetter . '0000';
    $first_name = $faker->firstName;
    $last_name = $faker->lastName;

    return [
        'attuid' => $attuid,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'full_name' => $first_name . ' ' . $last_name,
        'email' => $attuid . '@att.com',
        'password' => bcrypt('123456'), // password
        'remember_token' => Str::random(10),
        'enabled' => true,
    ];
});
