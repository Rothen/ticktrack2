<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExceptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exceptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('attuid', 10)->required();
            $table->string('full_name')->required();
            $table->string('description')->nullable();
            $table->string('distribution')->required();
            $table->foreign('distribution')->references('name')->on('distributions')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('tickets_per_emp')->unsigned()->required();
            $table->string('approver_attuid', 10)->required();
            $table->string('approver_full_name')->required();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exceptions');
    }
}
