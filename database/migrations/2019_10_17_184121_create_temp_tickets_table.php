<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ticketId')->required();
            $table->date('expiration')->nullable();
            $table->date('used')->nullable();
            $table->date('bought')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_tickets');
    }
}
