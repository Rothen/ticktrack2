<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('attuid', 10)->primary()->required();
            $table->string('first_name')->required();
            $table->string('last_name')->required();
            $table->string('full_name')->required();
            $table->string('email')->unique()->required();
            $table->string('password')->required()->default(bcrypt('123456'));
            $table->datetime('last_login')->nullable();
            $table->boolean('enabled')->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
