<?php

use App\Ticket;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

use Faker\Generator as Faker;

class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param Faker $faker
     * @return void
     */
    public function run(Faker $faker)
    {
        $values = [];
        for ($i = 0; $i < 10000; $i++) {
            // get a random digit, but always a new one, to avoid duplicates
            $values []= [
                'ticketId' => $faker->unique()->numberBetween(1000000000, 9999999999),
                'expiration' => Carbon::now()->endOfYear(),
                'bought' => Carbon::createFromDate(2019,2, 12),
                'created_at' => Carbon::now(),
            ];
        }

        Ticket::insert($values);
    }
}
