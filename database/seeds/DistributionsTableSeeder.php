<?php

use App\Distribution;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

use Faker\Generator as Faker;

class DistributionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param Faker $faker
     * @return void
     */
    public function run(Faker $faker)
    {
        $values = [
            /* ADMIN PERMISSIONS */
            [ 'display_name' => 'Apr 2019', 'from' => Carbon::createFromDate(2019,4, 3), 'tickets_per_emp' => 2, 'total_num_of_emp' => $faker->unique()->numberBetween(2500, 3000), 'status' => 'closed' ],
            [ 'display_name' => 'July 2019', 'from' => Carbon::createFromDate(2019,7, 14), 'tickets_per_emp' => 2, 'total_num_of_emp' => $faker->unique()->numberBetween(2500, 3000), 'status' => 'closed' ],
            [ 'display_name' => 'Sep 2019', 'from' => Carbon::createFromDate(2019,9, 22), 'tickets_per_emp' => 2, 'total_num_of_emp' => $faker->unique()->numberBetween(2500, 3000), 'status' => 'open' ],
        ];

        foreach ($values as $value) {
            (new Distribution)->create($value);
        }
    }
}
