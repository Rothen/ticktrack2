<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = ([
            [
                'attuid' => 'rr891c',
                'first_name' => 'Robert',
                'last_name' => 'Rothenstein',
                'full_name' => 'Robert Rothenstein',
                'email' => 'rr891c@att.com',
                'password' => bcrypt('123456'), // password
                'remember_token' => null,
            ],
            [
                'attuid' => 'kh574w',
                'first_name' => 'Karol',
                'last_name' => 'Hotovy',
                'full_name' => 'Karol Hotovy',
                'email' => 'kh574w@att.com',
                'password' => bcrypt('123456'), // password
                'remember_token' => null,
            ],
        ]);

        foreach ($values as $value) {
            (new User)->create($value);
        }
    }
}
