<?php

use App\Role;
use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = [
            // Admin
            [
                'role' => (new Role)->where('name', '=', 'admin')->first(),
                'permission' => [ 'see_php_info', 'see_roles', 'edit_roles', 'see_users', 'edit_users', 'see_distributions', 'edit_distributions', 'see_exceptions', 'edit_exceptions', 'import_tickets' ]
            ],
            // Employee
            [
                'role' => (new Role)->where('name', '=', 'employee')->first(),
                'permission' => [  ]
            ],
        ];

        foreach ($values as $value) {
            $value['role']->attachPermissions($value['permission']);
        }
    }
}
