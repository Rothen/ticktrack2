<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_assignments = [
            [ 'user' => (new User)->where('attuid', '=', 'rr891c')->first(), 'role' => (new Role)->where('name', '=', 'admin')->first()->name ],
            [ 'user' => (new User)->where('attuid', '=', 'kh574w')->first(), 'role' => (new Role)->where('name', '=', 'employee')->first()->name ],
        ];

        foreach ($role_assignments as $role_assignment) {
            $role_assignment['user']->attachRole($role_assignment['role']);
        }
    }
}
