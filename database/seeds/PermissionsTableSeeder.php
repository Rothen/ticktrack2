<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = [
            /* ADMIN PERMISSIONS */
            [ 'display_name' => 'See php info', 'category' => 'admin', 'description' => 'Enables to see php info' ],

            /* DISTRIBUTION MANAGEMENT */
            [ 'display_name' => 'See distributions', 'category' => 'distribution', 'description' => 'Enables to see distributions' ],
            [ 'display_name' => 'Edit distributions', 'category' => 'distribution', 'description' => 'Enables to edit distributions' ],
            [ 'display_name' => 'See exceptions', 'category' => 'exception', 'description' => 'Enables to see exceptions' ],
            [ 'display_name' => 'Edit exceptions', 'category' => 'exception', 'description' => 'Enables to edit exceptions' ],

            /** TOOL SETTINGS */
            [ 'display_name' => 'See roles', 'category' => 'role', 'description' => 'Enables to see roles' ],
            [ 'display_name' => 'Edit roles', 'category' => 'role', 'description' => 'Enables to edit roles - create, update, delete' ],
            [ 'display_name' => 'See users', 'category' => 'user', 'description' => 'Enables to see users' ],
            [ 'display_name' => 'Edit users', 'category' => 'user', 'description' => 'Enables to edit users' ],
            [ 'display_name' => 'Import tickets', 'category' => 'ticket', 'description' => 'Enables to import tickets' ],
        ];

        foreach ($values as $value) {
            (new Permission)->create($value);
        }
    }
}
