<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = [
            [
                'name' => 'admin',
                'display_name' => 'Admin',
                'description' => 'Can do anything'
            ],
            [
                'name' => 'employee',
                'display_name' => 'Employee',
                'description' => 'Can get benefit'
            ],
        ];

        foreach ($values as $value) {
            (new Role)->create($value);
        }
    }
}
