<?php

  header( "Expires: Fri, 07 Nov 1980 05:00:00 GMT" );
// HTTP/1.1
  header( "Cache-Control: no-store, no-cache, must-revalidate" );
  header( "Cache-Control: post-check=0, pre-check=0", false );
// HTTP/1.0
  header( "Pragma: no-cache" );
  
  error_reporting (0);

  switch (config('app.env')) {
      case 'development':
          $encReturnURL = urlencode("http://ticktrack-tst.intl.att.com/");
          break;
      case 'production':
          $encReturnURL = urlencode("http://ticktrack.intl.att.com/");
          break;
  }

 // $cspAuthURL = 'https://webtest.csp.att.com/empsvcs/hrpinmgt/pagLogin/?sysName=CapacityPlanner&retURL='.$encReturnURL;
 // $tooWeakURL = 'https://webtest.csp.att.com/empsvcs/hr/pagMenu_chgpin/?opt=12';
  $cspAuthURL = 'https://www.e-access.att.com/empsvcs/hrpinmgt/pagLogin/?sysName=dis&retURL='.$encReturnURL;
  // $tooWeakURL = 'https://www.e-access.att.com/empsvcs/hr/pagMenu_chgpin/?opt=12';
  
  if (! isset( $_COOKIE['attESSec'] ) ) {
  	header( 'HTTP/1.0 302 Redirect' );
  	header( "Location: ".$cspAuthURL );
  	header( "Connection: close" );
  }
  
  $attESSecRaw = $_COOKIE['attESSec'];
  $attESSecRaw = $attESSecRaw."\r\n";			//Add the carriage return and newline that PHP needs to make this work.

$decryptedSecCookie  = decryptCookie($attESSecRaw);
$secCookie = explode('|', $decryptedSecCookie);

/* Example of returned data ($secCookie):
    array("ANDRAS", "FEHER", "fa215v@intl.att.com", "421269257408 x57408", "", "rs802m", "", "fa215v,RDPLHYX,FT7CMM9,8808156", "YNNNYNYNNNNNNNNNNYNYNNNN", "", "ATFBD1000", "1");

    [5] is real attuid not cingular ID
*/

$attuid = $secCookie[5]; // this is real attuid not cingular ID

//echo "<br>Cookie: ".$attESSecRaw."<br>";

$attESHr = $_COOKIE['attESHr'];

$user_details = explode('|', $attESHr);
$ids = explode(',', $user_details[7]);

// $attid = strtolower($ids[0]); // this is retrieving cingular ID not attuid

	function decryptCookie( $rawCookie ) {
		$retVal = '';
		$service_port = 1500;
		$address = gethostbyname('localhost');
		$socket = socket_create (AF_INET, SOCK_STREAM, 0);
		if ($socket < 0) {
			echo "socket_create() failed: reason: ".
			socket_strerror ($socket) . "\n";
			return('');
		}
		$result = socket_connect ($socket, $address, $service_port);
		//echo "Cookie: ".$socket.", ".$address.", ".$service_port."<br>";
		//echo "Error code: ". socket_last_error($socket)."<br>";
		
		if ($result < 0) {
			echo "socket_connect() failed.\nReason: ($result) " . socket_strerror($result) . "\n";
			return('');
		}
		$in = $rawCookie;
		$out = '';
		socket_write ($socket, $in, strlen ($in));
		while ($out = socket_read ($socket, 2048)) {
			$retVal = $retVal.$out;
		}
		socket_close ($socket);
		return($retVal);
	}

	
?>