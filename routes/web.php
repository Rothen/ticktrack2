<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use App\Webphone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

Route::get('/', function () {

    if(isset(Auth::user()->attuid)) {
        return view('home');
    } elseif(in_array(config('app.env'), ['development', 'production'])) {
        include("../globallogon/glogon.php");
        if($attuid === 'rr891c') {
            $attuid = 'rr891c';
        }
        $user = (new User)->find($attuid);
        if ($user !== NULL) {
            if($user->enabled === true) {
                Auth::login($user, true); // login user and remember user as logged in
                return view('home');
            } else {
                abort(403);
            }
        } else {
            $employee = Webphone::where('SBCUID', '=', $attuid)->where('WORK_CITY', '=', 'BRATISLAVA')->first();
            if($employee) {
                $user = User::create([
                    'attuid' => $attuid,
                    'first_name' => $employee->FIRST_NAME,
                    'last_name' => $employee->LAST_NAME,
                    'full_name' => $employee->FIRST_NAME . ' ' . $employee->LAST_NAME,
                    'email' => $attuid . '@att.com',
                    'enabled' => true
                ]);

                $user->attachRole('employee');

                Auth::login($user, true); // login user and remember user as logged in
                return view('home');
            } else {
                abort(403, 'Only employees based in Bratislava have access');
            }
        }
    } else {
        return Redirect::to('login');
    }

});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->get('/{any?}', function () {
    return redirect()->route('home');
});

/* Admin routes */
Route::group(['prefix' => 'admin'], function() {
    Route::get('php_info', ['middleware' => ['permission:see_php_info'],'uses' => function () { echo phpinfo(); } ])->name('php_info');

    Route::post('log', ['uses' => 'LogController@store' ])->name('log.store');
});

/* ROLES */
Route::group(['prefix' => 'settings/roles'], function() {
    Route::get('/', ['middleware' => ['permission:see_roles'], 'uses' => 'RoleController@index'])->name('role.index');
    Route::get('/create', ['middleware' => ['permission:edit_roles'], 'uses' => 'RoleController@create'])->name('role.create');
    Route::post('/store', ['middleware' => ['permission:edit_roles'], 'uses' => 'RoleController@store'])->name('role.store');
    Route::get('/show/{role}', ['middleware' => ['permission:see_roles'], 'uses' => 'RoleController@show'])->name('role.show');
    Route::get('/edit/{role}', ['middleware' => ['permission:edit_roles'], 'uses' => 'RoleController@edit'])->name('role.edit');
    Route::patch('/update/{role}', ['middleware' => ['permission:edit_roles'], 'uses' => 'RoleController@update'])->name('role.update');
    Route::delete('/delete/{role}', ['middleware' => ['permission:edit_roles'], 'uses' => 'RoleController@destroy'])->name('role.destroy');

    Route::get('/assignees/{role}', [ 'middleware' => ['permission:see_roles'], 'uses' => 'RoleController@assignees'])->name('role.assignees');
    Route::get('/getRoleUnassignedPermissions/{role}', [ 'middleware' => ['permission:see_roles'], 'uses' => 'RoleController@getRoleUnassignedPermissions'])->name('role.getRoleUnassignedPermissions');
});

/* PERMISSIONS */
//Route::group(['prefix' => 'settings/permissions'], function() {
//    Route::get('/', ['middleware' => ['permission:edit_roles'], 'uses' => 'PermissionController@index'])->name('permission.index');
//    Route::get('/show/{permission}', ['middleware' => ['permission:edit_roles'], 'uses' => 'PermissionController@show'])->name('permission.show');
//});

/* USERS */
Route::group(['prefix' => 'settings/users'], function() {
    Route::get('/', ['middleware' => ['permission:see_users'], 'uses' => 'UserController@index'])->name('user.index');
    Route::post('/store', ['middleware' => ['permission:edit_users'], 'uses' => 'UserController@store'])->name('user.store');
    Route::get('/show/{attuid}', ['middleware' => ['permission:see_users'], 'uses' => 'UserController@show'])->name('user.show');
    Route::get('/edit/{attuid}', ['middleware' => ['permission:edit_users'], 'uses' => 'UserController@edit'])->name('user.edit');
    Route::patch('/update/{attuid}', ['middleware' => ['permission:edit_users'], 'uses' => 'UserController@update'])->name('user.update');
    Route::patch('/disable/{attuid}', ['middleware' => ['permission:edit_users'], 'uses' => 'UserController@disable'])->name('user.disable');
    Route::delete('/delete/{attuid}', ['middleware' => ['permission:edit_users'], 'uses' => 'UserController@destroy'])->name('user.destroy');

    Route::get('/searchEmployee/{searchString}', ['middleware' => ['permission:see_users'], 'uses' => 'UserController@searchEmployee'])->name('user.searchEmployee');
    Route::get('/searchUser/{searchString}', ['middleware' => ['permission:see_users'], 'uses' => 'UserController@searchUser'])->name('user.searchUser');
    Route::get('/getUser', [ 'uses' => 'UserController@getUser'])->name('user.getUser');
});

/* BarCode */
Route::group(['prefix' => 'barcode'], function() {
    Route::get('/show', [ 'uses' => 'BarCodeController@show'])->name('barcode.show');
    Route::get('/sendBarCodesInMail', [ 'uses' => 'BarCodeController@sendBarCodesInMail'])->name('barcode.sendBarCodesInMail');
    Route::get('/generateBarCodesToPDF', [ 'uses' => 'BarCodeController@generateBarCodesToPDF'])->name('barcode.generateBarCodesToPDF');
});

/* TICKETS */
Route::group(['prefix' => 'ticket'], function() {
    Route::get('/eligibleTickets', [ 'uses' => 'TicketController@retrieveEmployeeEligibleTickets'])->name('ticket.retrieveEmployeeEligibleTickets');
    Route::get('/userTicketsUsageOverview', [ 'uses' => 'TicketController@userTicketsUsageOverview'])->name('ticket.userTicketsUsageOverview');
    Route::get('/getTickets', [ 'uses' => 'TicketController@getTickets'])->name('ticket.getTickets');
    Route::post('/importTickets', [ 'uses' => 'TicketController@import'])->name('ticket.import');
});

/* USER NOTIFICATIONS */
Route::group(['prefix' => 'userNotifications'], function() {
    Route::post('/store', [ 'uses' => 'UserNotificationController@store'])->name('userNotification.store');
    Route::patch('/update/{user}', [ 'uses' => 'UserNotificationController@update'])->name('userNotification.update');
});

/* DISTRIBUTIONS */
Route::group(['prefix' => 'distributions'], function() {
    Route::get('/overview', [ 'middleware' => ['permission:see_distributions'], 'uses' => 'DistributionController@index'])->name('distribution.index');
    Route::get('/actual', [ 'uses' => 'DistributionController@actualDistribution'])->name('distribution.actual');
    Route::post('/store', [ 'middleware' => ['permission:edit_distributions'],'uses' => 'DistributionController@store'])->name('distribution.store');
    Route::patch('/update/{distribution}', [ 'middleware' => ['permission:edit_distributions'], 'uses' => 'DistributionController@update'])->name('distribution.update');
});

/* EXCEPTIONS */
Route::group(['prefix' => 'exceptions'], function() {
    Route::get('/actual', [ 'middleware' => ['permission:see_exceptions'], 'uses' => 'ExceptionController@actual'])->name('exception.actual');
    Route::get('/show/{distribution}', [ 'middleware' => ['permission:see_exceptions'], 'uses' => 'ExceptionController@show'])->name('exception.show');
    Route::post('/store', [ 'middleware' => ['permission:edit_exceptions'],'uses' => 'ExceptionController@store'])->name('exception.store');
    Route::post('/copyExceptions', [ 'middleware' => ['permission:edit_exceptions'],'uses' => 'ExceptionController@copyExceptions'])->name('exception.copyExceptions');
    Route::patch('/update/{exception}', [ 'middleware' => ['permission:edit_exceptions'], 'uses' => 'ExceptionController@update'])->name('exception.update');
    Route::delete('/delete/{exception}', ['middleware' => ['permission:edit_exceptions'], 'uses' => 'ExceptionController@destroy'])->name('exception.destroy');
});