module.exports = {
  theme: {
    extend: {},
    minWidth: {
      '0': '0'
      , '100': '100px'
      , '150': '150px'
      , '200': '200px'
      , '1/4': '25%'
      , '1/2': '50%'
      , '3/4': '75%'
      , 'full': '100%'
      , '70vh': '70vh'
      , '75vh': '75vh'
      , '80vh': '80vh'
      , '100vh': '100vh'
    },
    maxWidth: {
      '0': '0'
      , 'xs': '20rem'
      , 'sm': '24rem'
      , 'md': '28rem'
      , 'lg': '32rem'
      , 'xl': '36rem'
      , '2xl': '42rem'
      , '3xl': '48rem'
      , '4xl': '56rem'
      , '5xl': '64rem'
      , '6xl': '72rem'
      , '90p': '90%'
      , 'full': '100%'
      , '50vh': '50vw'
      , '70vh': '70vw'
      , '75vh': '75vw'
      , '80vh': '80vw'
      , '90vh': '90vw'
      , '100vh': '100vw'
    },
    maxHeight: {
      '0': '0'
      , '70vh': '70vh'
      , '75vh': '75vh'
      , '80vh': '80vh'
      , '100vh': '100vh'
      , '1/4': '25%'
      , '2/4': '50%'
      , '3/4': '75%'
      , 'full': '100%'
    },
    cursor: {
          auto: 'auto',
          default: 'default',
          pointer: 'pointer',
          wait: 'wait',
          text: 'text',
          move: 'move',
          'not-allowed': 'not-allowed',
          crosshair: 'crosshair',
          'zoom-in': 'zoom-in',
          help: 'help'
}
  },
  variants: {},
  plugins: []
}
