import { library } from '@fortawesome/fontawesome-svg-core'
import { faAlignLeft, faPencilAlt, faTimes, faUsers, faEyeSlash, faUnlockAlt, faPlus, faHandPointer, faTrashAlt, faArrowLeft, faChevronCircleRight, faChevronCircleLeft, faChevronCircleUp, faChevronCircleDown, faInfoCircle, faQuestionCircle, faTimesCircle, faPlayCircle, faCopy } from '@fortawesome/free-solid-svg-icons'
// import { faHandPointer } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'

library.add(faAlignLeft, faPencilAlt, faTimes, faUsers, faEyeSlash, faUnlockAlt, faPlus, faHandPointer, faTrashAlt, faArrowLeft, faChevronCircleRight, faChevronCircleLeft, faChevronCircleUp, faChevronCircleDown, faInfoCircle, faQuestionCircle, faTimesCircle, faPlayCircle, faCopy );

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('font-awesome-layers', FontAwesomeLayers);
Vue.component('font-awesome-layers-text', FontAwesomeLayersText);

Vue.config.productionTip = false;