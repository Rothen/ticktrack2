const namespaced = true;

const state = {
    notifications: []
};

const getters = {
    notifications: state => {
        return state.notifications;
    }
};

const mutations = {
    showNotification(state, notification) {
        notification.key = Date.now();
        state.notifications.push(notification);
    },
    removeNotification(state, notification) {
        state.notifications.splice(state.notifications.findIndex(v => v.text === notification.text), 1)
    }
};

const actions = {
    showNotification({ commit }, notification){
        this.commit('notificationModule/showNotification', notification);
    },
    removeNotification({ commit }, notification){
        this.commit('notificationModule/removeNotification', notification);
    }
};

export default {
    namespaced,
    state,
    mutations,
    actions,
    getters
}