const namespaced = true;

const state = {
    error: null
};

const getters = {
    errors: state => {
        return state.error;
    }
};

const mutations = {
    showError(state, error) {
        state.error = error;
    },
    hideError(state) {
        state.error = null;
    }
};

const actions = {
    showError({ commit }, error){
        switch (error.error.response.status.toString()) {
            case '401':
                this.commit('errorModule/showError', {
                        errorMessage: 'You could not be logged in.',
                        actionMessage: 'Please, refresh the page or log in via Global Logon. If it will not help, probably your user profile is disabled. Contact your manager. Admin will NOT be able to help you, as Admin can NOT decide whether your user profile should be activated.'
                    }
                );
                break;
            case '403':
                this.commit('errorModule/showError', {
                        errorMessage: 'You\'re not authorized to see these data.',
                        actionMessage: 'Please, contact your manager. Admin will NOT be able to help you, as Admin can NOT decide whether you should see these data.'
                    }
                );
                break;
            case '404':
                this.commit('errorModule/showError', {
                        errorMessage: 'The page you\'ve tried to access does not exist.',
                        actionMessage: 'Please, check the link (url) and refresh the page'
                    }
                );
                break;
            case '422':
                break;
            default:
                this.commit('errorModule/showError', {
                        errorMessage: 'Something went wrong.',
                        actionMessage: 'If you believe, pray. If not, hope. In both cases, refresh the page.'
                    }
                );
                break;
        }

    },
    hideError({ commit }){
        this.commit('errorModule/hideError');
    }
};

export default {
    namespaced,
    state,
    mutations,
    actions,
    getters
}