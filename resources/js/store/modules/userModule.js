import axios from "axios";

const namespaced = true;

const state = {
    // expires_in: 0,
    // tokenId: null,
    // user: null,
    // headers: []
};

const getters = {
    // isAuthenticated(state, getters, rootState) {
    //     return rootState.userModule.tokenId !== null;
    // },
    // userFirstName(state, getters, rootState) {
    //     if(rootState.userModule.user) {
    //         return rootState.userModule.user.first_name;
    //     }
    // },
    // getUserData(state, getters, rootState){
    //     if(rootState.userModule.user) {
    //         return rootState.userModule.user;
    //     }
    // }
};

const mutations = {
    // storeAuthData: (state, responseData) => {
    //     state.tokenId = responseData.tokenId;
    //     state.expires_in = responseData.expires_in;
    //     let now = new Date();
    //     let expirationDate = new Date(now.getTime() + (responseData.expires_in * 1000));
    //     localStorage.setItem('tokenId', responseData.tokenId);
    //     localStorage.setItem('expirationDate', expirationDate);
    // },
    // storeUserData: (state, responseData) => {
    //     state.user = responseData.user;
    // },
    // updateHeaders: (state) => {
    //     let cols = JSON.parse(state.user.rds_columns);
    //     let cols_length = Object.keys(cols).length;
    //     let i = 1;
    //     let custom_headers = [];
    //     for (i; i <= cols_length; i++) {
    //         custom_headers.push({text: cols[i], value: cols[i]});
    //     }
    //     state.headers = custom_headers;
    // },
    // clearLocalStorageUserData: () => {
    //     localStorage.removeItem('tokenId');
    //     localStorage.removeItem('expirationDate');
    // },
    // clearStateUserData: (state) => {
    //     state.tokenId = null;
    //     state.expires_in = null;
    //     state.user = null;
    //     state.headers = null;
    // },
};

const actions = {
    login ({ commit, dispatch }, data) {
        axios
            .post('api/auth/login', {
                attuid: data.attuid,
                password: '123456',
                enabled: data.enabled
            })
            .then(function (response) {
                // console.log(response.data);
                // console.log('aaaa');
                // commit('userModule/storeAuthData', { tokenId: response.data.access_token, expires_in: response.data.expires_in } );
                // dispatch('storeUserData', response.data.access_token);
            })
            .catch(error => {
                this.dispatch('logModule/storeLog', { error: error });
                this.dispatch('notificationModule/showNotification', {notificationVisibility: true, title: 'Your user access is disabled', text: 'Please contact your manager', background: 'orange' });
            });
    },
    // tryAutoLogin({ commit, dispatch }, data){
    //     const tokenId = localStorage.getItem('tokenId');
    //     if(!tokenId){
    //         this.dispatch('userModule/login', data );
    //         // window.location.replace(route('login'));
    //         return
    //     }
    //     const now = new Date();
    //     const expirationDate = localStorage.getItem('expirationDate');
    //     if(expirationDate <= now){
    //         window.location.replace(route('login'));
    //         return
    //     }
    //     // this.commit('storeAuthData', { tokenId: tokenId, expires_in: 3600  } );
    //     // this.dispatch('userModule/login', data );
    // },
    // storeUserData ({ commit, state }, data ) {
    //     if(!state.tokenId) {
    //         return
    //     }
    //     axios
    //         .post('api/auth/me', {
    //             token: data
    //         })
    //         .then(function (response) {
    //             commit('storeUserData', { user: response.data } );
    //         })
    // },
    // logout() {
    //     this.commit('clearLocalStorageUserData');
    //     // this.commit('clearStateUserData');
    // }
};

export default {
    namespaced,
    state,
    getters,
    mutations,
    actions
}