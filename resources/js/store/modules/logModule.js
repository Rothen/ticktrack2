const namespaced = true;

const state = {
};

const getters = {
};

const mutations = {
    hideError(state) {
        state.error = null;
    }
};

const actions = {
    storeLog({ commit }, data ){
        if(data.error.response.status.toString()) {
            this.dispatch('errorModule/showError', data);
            if (data.error.response.status.toString() !== '422') {
                axios.post(route('log.store'), {
                    type: 'response error',
                    statusCode: data.error.response.status.toString(),
                })
                .then(response => {

                })
            }
        }
    }
};

export default {
    namespaced,
    state,
    mutations,
    actions,
    getters
}