import Vue from 'vue';
import Vuex from 'vuex';
import notificationModule from './modules/notificationModule'
import errorModule from './modules/errorModule'
import logModule from './modules/logModule'
import userModule from './modules/userModule'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {

    },
    getters: {

    },
    mutations: {

    },

    actions: {

    },
    modules: {
        notificationModule, errorModule, logModule, userModule
    },
    methods: {

    }
});