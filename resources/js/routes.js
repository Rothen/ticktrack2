import Home from "./components/home";

// USER SETTINGS
import UserNotificationSetting from "./components/settings/userNotifications/userNotificationSetting";

// DISTRIBUTION MANAGEMENT
import Distributions from "./components/settings/distributions/distributions";
import DistributionCreate from "./components/settings/distributions/distributionCreate";
import Exceptions from "./components/settings/distributions/exceptions/exceptions";

// TOOL SETTINGS
import Roles from "./components/settings/roles/Roles";
import RoleCreate from "./components/settings/roles/RoleCreate";
import RoleEdit from "./components/settings/roles/RoleEdit";
import Users from "./components/settings/users/users";
import UserCreate from "./components/settings/users/userCreate";
import UserEdit from "./components/settings/users/userEdit";
import ImportTickets from "./components/settings/tickets/importTickets";

export default {
    // mode: 'history',

    routes: [
        { path: '/error', component: Error, name: 'error', props: true },
        { path: '/home', component: Home, name: 'home', props: true },

        // USER SETTINGS
        { path: '/userNotificationSetting', component: UserNotificationSetting, name: 'userNotificationSetting', props: true },

        // DISTRIBUTION MANAGEMENT
        { path: '/distributions', component: Distributions, name: 'distributions', props: true },
        { path: '/distributionCreate', component: DistributionCreate, name: 'distributionCreate', props: true },
        { path: '/exceptions', component: Exceptions, name: 'exceptions', props: true },

        // TOOL SETTINGS
        { path: '/', component: Home, name: 'main', props: true },
        { path: '/roles', component: Roles, name: 'roles', props: true },
        { path: '/createRole', component: RoleCreate, name: 'roleCreate', props: true },
        { path: '/editRole', component: RoleEdit, name: 'roleEdit', props: true },
        { path: '/users', component: Users, name: 'users', props: true },
        { path: '/addUser', component: UserCreate, name: 'userCreate', props: true },
        { path: '/editUser', component: UserEdit, name: 'userEdit', props: true },
        { path: '/importTickets', component: ImportTickets, name: 'importTickets', props: true },
    ]
}