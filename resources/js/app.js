/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import routes from './routes';
import { store } from './store/store.js';
require('./icons');

Vue.use(VueRouter);
const router = new VueRouter(routes);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('mainComponent', require('./components/mainComponent.vue').default);
Vue.component('navigation', require('./components/navigation.vue').default);
Vue.component('dropdown', require('./components/utilities/htmlElements/dropdown').default);
Vue.component('notifications-list', require('./components/utilities/notifications/notificationsList').default);
Vue.component('error', require('./components/error').default);
Vue.component('loading', require('./components/utilities/loading').default);

import Permissions from './mixins/Permissions';
import myMethods from './mixins/myMethods';
Vue.mixin(Permissions);
Vue.mixin(myMethods);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store
});
