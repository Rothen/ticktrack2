@component('mail::message')

<div style="text-align: center">
    <div style="margin-bottom: 20px">
        <div><strong>K dispozícii sú nové kupóny do Cinema City</strong></div>
        @if($distribution->from)
        <div>Začiatok distribúcie: {{ $distribution->from->locale('sk_Sk')->isoFormat('dddd Do. MMMM YYYY') }}</div>
        @endif
    </div>

    @component('mail::button', ['url' => config('app.url') ])
        {{ 'Pozrieť lístky' }}
    @endcomponent

</div>

@endcomponent