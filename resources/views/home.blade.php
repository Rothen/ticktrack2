@extends('layouts.app')

@section('content')
    <div class="section pb-10">
        <div class="container mx-auto">
            <main class="">
                <main-component></main-component>
            </main>
        </div>
    </div>
@endsection
