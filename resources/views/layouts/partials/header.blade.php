<nav class="bg-blue-900 shadow mb-8 py-6">
    <div class="container mx-auto px-6 md:px-0">
        <div class="flex items-center justify-center">
            <div class="mr-6">
                <a href="{{ url('/') }}" class="text-lg font-semibold text-gray-100 no-underline">
                    {{ config('app.name', 'CTA') }}
                </a>
            </div>
            <div class="flex-1 text-right">
                @guest
                    <a class="no-underline hover:underline text-gray-300 text-sm p-3" href="{{ route('login') }}">{{ __('Login') }}</a>
                    @if (Route::has('register'))
                        <a class="no-underline hover:underline text-gray-300 text-sm p-3" href="{{ route('register') }}">{{ __('Register') }}</a>
                    @endif
                @else
                    <dropdown>
                        <span slot="menu_name" class="appearance-none flex text-red items-center inline-block text-white font-medium bg-blue hover:bg-blue-dark px-4 py-2 rounded">
                          <span class="text-gray-300 text-sm pr-1">{{ $authenticated_user->full_name }}</span>
                          <svg class="h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                          </svg>
                        </span>
                        <div slot="links" class="bg-white shadow rounded border overflow-hidden text-left py-2">
                            @has_permission('see_php_info', $users_permissions)
                            <a class="no-underline block py-1 px-5 text-sm text-grey-darkest hover:bg-blue-100 whitespace-no-wrap" href="{{ route('php_info') }}" target="_blank">PHP info</a>
                            @endhas_permission
                            <a href="{{ route('logout') }}"
                               class="no-underline block py-1 px-5 text-sm text-grey-darkest hover:bg-blue-100 whitespace-no-wrap"
                               onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                    {{ csrf_field() }}
                                </form>
                        </div>
                    </dropdown>
                @endguest
            </div>
        </div>
    </div>
</nav>