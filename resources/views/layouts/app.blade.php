<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TickTrack') }}</title>

    <!-- Styles -->
    <link href="{{ url('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-gray-100 h-screen antialiased leading-none">

    <div id="app">

        @include('layouts.partials.header')

        @yield('content')

    </div>

    <!-- Scripts -->

    {{--Ziggy plugin directive--}}
    @routes

    <script>
        @auth
            window.Permissions = {!! json_encode(Auth::user()->allPermissions()->pluck('name')->toArray(), true) !!};
            window.AuthenticatedUser = {!! json_encode(Auth::user(), true) !!};
        @else
            window.Permissions = [];
            window.AuthenticatedUser = [];
        @endauth
    </script>

    <script src="{{ url('js/app.js') }}"></script>
</body>
</html>
